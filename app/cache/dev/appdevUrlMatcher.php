<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appdevUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appdevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);

        // _wdt
        if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?<token>[^/]+)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::toolbarAction',)), array('_route' => '_wdt'));
        }

        if (0 === strpos($pathinfo, '/_profiler')) {
            // _profiler_search
            if ($pathinfo === '/_profiler/search') {
                return array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::searchAction',  '_route' => '_profiler_search',);
            }

            // _profiler_purge
            if ($pathinfo === '/_profiler/purge') {
                return array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::purgeAction',  '_route' => '_profiler_purge',);
            }

            // _profiler_info
            if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?<about>[^/]+)$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::infoAction',)), array('_route' => '_profiler_info'));
            }

            // _profiler_import
            if ($pathinfo === '/_profiler/import') {
                return array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::importAction',  '_route' => '_profiler_import',);
            }

            // _profiler_export
            if (0 === strpos($pathinfo, '/_profiler/export') && preg_match('#^/_profiler/export/(?<token>[^/\\.]+)\\.txt$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::exportAction',)), array('_route' => '_profiler_export'));
            }

            // _profiler_phpinfo
            if ($pathinfo === '/_profiler/phpinfo') {
                return array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::phpinfoAction',  '_route' => '_profiler_phpinfo',);
            }

            // _profiler_search_results
            if (preg_match('#^/_profiler/(?<token>[^/]+)/search/results$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::searchResultsAction',)), array('_route' => '_profiler_search_results'));
            }

            // _profiler
            if (preg_match('#^/_profiler/(?<token>[^/]+)$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::panelAction',)), array('_route' => '_profiler'));
            }

            // _profiler_redirect
            if (rtrim($pathinfo, '/') === '/_profiler') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', '_profiler_redirect');
                }

                return array (  '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\RedirectController::redirectAction',  'route' => '_profiler_search_results',  'token' => 'empty',  'ip' => '',  'url' => '',  'method' => '',  'limit' => '10',  '_route' => '_profiler_redirect',);
            }

        }

        if (0 === strpos($pathinfo, '/_configurator')) {
            // _configurator_home
            if (rtrim($pathinfo, '/') === '/_configurator') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', '_configurator_home');
                }

                return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
            }

            // _configurator_step
            if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?<index>[^/]+)$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',)), array('_route' => '_configurator_step'));
            }

            // _configurator_final
            if ($pathinfo === '/_configurator/final') {
                return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
            }

        }

        // analyst_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?<name>[^/]+)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Bonilla\\AnalystBundle\\Controller\\DefaultController::indexAction',)), array('_route' => 'analyst_homepage'));
        }

        if (0 === strpos($pathinfo, '/especie')) {
            // especie
            if (rtrim($pathinfo, '/') === '/especie') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'especie');
                }

                return array (  '_controller' => 'Bonilla\\AnalystBundle\\Controller\\EspecieController::indexAction',  '_route' => 'especie',);
            }

            // especie_show
            if (preg_match('#^/especie/(?<id>[^/]+)/show$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Bonilla\\AnalystBundle\\Controller\\EspecieController::showAction',)), array('_route' => 'especie_show'));
            }

            // especie_new
            if ($pathinfo === '/especie/new') {
                return array (  '_controller' => 'Bonilla\\AnalystBundle\\Controller\\EspecieController::newAction',  '_route' => 'especie_new',);
            }

            // especie_create
            if ($pathinfo === '/especie/create') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_especie_create;
                }

                return array (  '_controller' => 'Bonilla\\AnalystBundle\\Controller\\EspecieController::createAction',  '_route' => 'especie_create',);
            }
            not_especie_create:

            // especie_edit
            if (preg_match('#^/especie/(?<id>[^/]+)/edit$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Bonilla\\AnalystBundle\\Controller\\EspecieController::editAction',)), array('_route' => 'especie_edit'));
            }

            // especie_update
            if (preg_match('#^/especie/(?<id>[^/]+)/update$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_especie_update;
                }

                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Bonilla\\AnalystBundle\\Controller\\EspecieController::updateAction',)), array('_route' => 'especie_update'));
            }
            not_especie_update:

            // especie_delete
            if (preg_match('#^/especie/(?<id>[^/]+)/delete$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_especie_delete;
                }

                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Bonilla\\AnalystBundle\\Controller\\EspecieController::deleteAction',)), array('_route' => 'especie_delete'));
            }
            not_especie_delete:

        }

        if (0 === strpos($pathinfo, '/reporte')) {
            // reporte
            if (rtrim($pathinfo, '/') === '/reporte') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'reporte');
                }

                return array (  '_controller' => 'Bonilla\\AnalystBundle\\Controller\\ReporteController::indexAction',  '_route' => 'reporte',);
            }

            // reporte_show
            if (preg_match('#^/reporte/(?<id>[^/]+)/show$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Bonilla\\AnalystBundle\\Controller\\ReporteController::showAction',)), array('_route' => 'reporte_show'));
            }

            // reporte_new
            if ($pathinfo === '/reporte/new') {
                return array (  '_controller' => 'Bonilla\\AnalystBundle\\Controller\\ReporteController::newAction',  '_route' => 'reporte_new',);
            }

            // reporte_create
            if ($pathinfo === '/reporte/create') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_reporte_create;
                }

                return array (  '_controller' => 'Bonilla\\AnalystBundle\\Controller\\ReporteController::createAction',  '_route' => 'reporte_create',);
            }
            not_reporte_create:

            // reporte_edit
            if (preg_match('#^/reporte/(?<id>[^/]+)/edit$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Bonilla\\AnalystBundle\\Controller\\ReporteController::editAction',)), array('_route' => 'reporte_edit'));
            }

            // reporte_update
            if (preg_match('#^/reporte/(?<id>[^/]+)/update$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_reporte_update;
                }

                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Bonilla\\AnalystBundle\\Controller\\ReporteController::updateAction',)), array('_route' => 'reporte_update'));
            }
            not_reporte_update:

            // reporte_delete
            if (preg_match('#^/reporte/(?<id>[^/]+)/delete$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_reporte_delete;
                }

                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Bonilla\\AnalystBundle\\Controller\\ReporteController::deleteAction',)), array('_route' => 'reporte_delete'));
            }
            not_reporte_delete:

        }

        // example1
        if (rtrim($pathinfo, '/') === '/example1') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'example1');
            }

            return array (  '_controller' => 'Bonilla\\AnalystBundle\\Controller\\DefaultController::example1Action',  '_route' => 'example1',);
        }

        // registrar_temperatura
        if (rtrim($pathinfo, '/') === '/registrar_temperatura') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'registrar_temperatura');
            }

            return array (  '_controller' => 'Bonilla\\AnalystBundle\\Controller\\TemperaturaController::registrarTemperaturaAction',  '_route' => 'registrar_temperatura',);
        }

        // registrar_mejor_precio_vuelo
        if (0 === strpos($pathinfo, '/registrar_mejor_precio_vuelo') && preg_match('#^/registrar_mejor_precio_vuelo/(?<fecha>[^/]+)/(?<origen>[^/]+)/(?<destino>[^/]+)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Bonilla\\AnalystBundle\\Controller\\PrecioVueloController::registrarMejorPrecioVueloAction',)), array('_route' => 'registrar_mejor_precio_vuelo'));
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
