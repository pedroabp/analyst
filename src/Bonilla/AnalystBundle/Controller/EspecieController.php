<?php

namespace Bonilla\AnalystBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Bonilla\AnalystBundle\Entity\Especie;
use Bonilla\AnalystBundle\Form\EspecieType;

/**
 * Especie controller.
 *
 */
class EspecieController extends Controller
{
    /**
     * Lists all Especie entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AnalystBundle:Especie')->findAll();

        return $this->render('AnalystBundle:Especie:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Especie entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnalystBundle:Especie')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Especie entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AnalystBundle:Especie:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to create a new Especie entity.
     *
     */
    public function newAction()
    {
        $entity = new Especie();
        $form   = $this->createForm(new EspecieType(), $entity);

        return $this->render('AnalystBundle:Especie:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new Especie entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Especie();
        $form = $this->createForm(new EspecieType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('especie_show', array('id' => $entity->getId())));
        }

        return $this->render('AnalystBundle:Especie:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Especie entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnalystBundle:Especie')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Especie entity.');
        }

        $editForm = $this->createForm(new EspecieType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AnalystBundle:Especie:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Especie entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnalystBundle:Especie')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Especie entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new EspecieType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('especie_edit', array('id' => $id)));
        }

        return $this->render('AnalystBundle:Especie:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Especie entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AnalystBundle:Especie')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Especie entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('especie'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
