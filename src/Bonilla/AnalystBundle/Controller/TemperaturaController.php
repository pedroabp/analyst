<?php

namespace Bonilla\AnalystBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Bonilla\AnalystBundle\Entity\Registrotemperatura;
use Bonilla\AnalystBundle\Form\ReporteType;

use \DateTime;
use \SoapClient;

class TemperaturaController extends Controller
{
    public static function obtenerTemperatura()
    {
        try
        {
            $options = array();
            $options['trace'] = TRUE;

            $requestParams = array('CityName' => 'Cali / Alfonso Bonillaaragon', 'CountryName' => 'Colombia');

            $client = new SoapClient('http://www.webservicex.net/globalweather.asmx?WSDL', $options);
            $response = $client->GetWeather($requestParams);

            $xmlElement = simplexml_load_string(mb_convert_encoding($response->GetWeatherResult, 'UTF-16', 'UTF-8'));
            $temperatura = $xmlElement->Temperature;
            $temperatura = explode('(', $temperatura);
            $temperatura = explode(' ', $temperatura[1]);
            $temperatura = $temperatura[0];

            return $temperatura;
        } catch(Exception $e)
        {
            echo $e->getMessage() . "\n";
            return FALSE;
        }
    }

    public function registrarTemperaturaAction()
    {
        $em = $this->getDoctrine()->getManager();

        $registro = new Registrotemperatura();

        $datetime = new DateTime('now');
        $registro->setTemFecha($datetime);
        $registro->setTemHora($datetime->format('H'));

        $temperatura = TemperaturaController::obtenerTemperatura();

        $registro->setTemTemperatura(floatval($temperatura));

        $em->persist($registro);
        $em->flush();

        return new Response($temperatura);
    }

}
