<?php

namespace Bonilla\AnalystBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Bonilla\AnalystBundle\Entity\Reporte;

use \Graph;
use \UniversalTheme;
use \LinePlot;

class DefaultController extends Controller
{
    public function example1Action()
    {
        $pChartPath = getcwd() . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'jpgraph' . DIRECTORY_SEPARATOR;
        $path = $pChartPath . 'src/jpgraph.php';
        include_once $path;
        $path = $pChartPath . 'src/jpgraph_line.php';
        include_once $path;

        // require_once ('jpgraph/jpgraph.php');
        // require_once ('jpgraph/jpgraph_line.php');

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AnalystBundle:Reporte')->findAll();

        $datay1 = array();
        $datax = array();
        foreach ($entities as $entity)
        {
            // $entity = new Reporte();
            $datay1[] = $entity->getRepPatrimonio();
            $datax[] = $entity->getRepAnio() . 'T' . $entity->getRepTrimestre();
        }

        $datay2 = array(12, 9, 42, 8);
        $datay3 = array(5, 17, 32, 24);

        // Setup the graph
        $graph = new Graph(1024, 600);
        $graph->SetScale("textlin");

        $theme_class = new UniversalTheme;

        $graph->SetTheme($theme_class);
        $graph->img->SetAntiAliasing(false);
        $graph->title->Set('Patrimonio');
        $graph->SetBox(false);

        $graph->img->SetAntiAliasing();
        $graph->img->SetMargin(100,50,0,0);

        $graph->yaxis->HideZeroLabel();
        $graph->yaxis->HideLine(false);
        $graph->yaxis->HideTicks(false, false);

        $graph->xgrid->Show();
        $graph->xgrid->SetLineStyle("solid");
        $graph->xaxis->SetTickLabels($datax);
        $graph->xgrid->SetColor('#E3E3E3');

        // Create the first line
        $p1 = new LinePlot($datay1);
        $graph->Add($p1);
        $p1->SetColor("#6495ED");
        $p1->SetLegend('Line 1');

        $graph->legend->SetFrameWeight(1);

        // Output line
        $graph->Stroke();
        die();
    }

    public function indexAction($name)
    {
        return $this->render('AnalystBundle:Default:index.html.twig', array('name' => $name));
    }

}
