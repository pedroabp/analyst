<?php

namespace Bonilla\AnalystBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Bonilla\AnalystBundle\Entity\Reporte;
use Bonilla\AnalystBundle\Form\ReporteType;

/**
 * Reporte controller.
 *
 */
class ReporteController extends Controller
{
    /**
     * Lists all Reporte entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AnalystBundle:Reporte')->findAll();

        return $this->render('AnalystBundle:Reporte:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Reporte entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnalystBundle:Reporte')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Reporte entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AnalystBundle:Reporte:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to create a new Reporte entity.
     *
     */
    public function newAction()
    {
        $entity = new Reporte();
        $form   = $this->createForm(new ReporteType(), $entity);

        return $this->render('AnalystBundle:Reporte:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new Reporte entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Reporte();
        $form = $this->createForm(new ReporteType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('reporte_show', array('id' => $entity->getId())));
        }

        return $this->render('AnalystBundle:Reporte:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Reporte entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnalystBundle:Reporte')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Reporte entity.');
        }

        $editForm = $this->createForm(new ReporteType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AnalystBundle:Reporte:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Reporte entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnalystBundle:Reporte')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Reporte entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new ReporteType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('reporte_edit', array('id' => $id)));
        }

        return $this->render('AnalystBundle:Reporte:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Reporte entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AnalystBundle:Reporte')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Reporte entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('reporte'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
