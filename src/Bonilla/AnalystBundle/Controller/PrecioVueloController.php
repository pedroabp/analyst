<?php

namespace Bonilla\AnalystBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Bonilla\AnalystBundle\Entity\Registropreciovuelo;
use Bonilla\AnalystBundle\Form\ReporteType;

use \DateTime;
use \SoapClient;

class PrecioVueloController extends Controller
{
    public static function obtenerMejorPrecioVuelo($datetime, $origen, $destino)
    {
        try
        {
            $ch = curl_init();
			$url = "http://www.despegar.com.co/shop/flights/data/search/oneway/$origen/$destino/" . $datetime->format('Y-m-d') . '/1/0/0/TOTALFARE/ASCENDING/NA/NA/NA/NA';
            curl_setopt($ch, CURLOPT_URL, $url);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $response = curl_exec($ch);
            // echo $response;
            curl_close($ch);

            $arreglo = json_decode($response, TRUE);
            // var_dump($arreglo);

            $mejorPrecio = str_replace('.', '', $arreglo['result']['data']['pricesSummary']['bestPrice'][0]['formatted']['amount']);
            $mejorPrecio = intval($mejorPrecio);

            $arregloResultado = array();

            $arregloResultado['bestPrice'] = $mejorPrecio;

            $aerolineas = $arreglo['result']['data']['pricesSummary']['matrix'];
            foreach ($aerolineas as $aerolinea)
            {
                if ($aerolinea['noScale'] != NULL && $aerolinea['noScale']['bestPrice'] == TRUE)
                {
                    $arregloResultado['airline'] = $aerolinea['airline']['description'];
                    $arregloResultado['scales'] = 0;
                    break;
                }
                if ($aerolinea['oneScale'] != NULL && $aerolinea['oneScale']['bestPrice'] == TRUE)
                {
                    $arregloResultado['airline'] = $aerolinea['airline']['description'];
                    $arregloResultado['scales'] = 1;
                    break;
                }
                if ($aerolinea['twoPlusScales'] != NULL && $aerolinea['twoPlusScales']['bestPrice'] == TRUE)
                {
                    $arregloResultado['airline'] = $aerolinea['airline']['description'];
                    $arregloResultado['scales'] = 2;
                    break;
                }
            }

            return $arregloResultado;
        } catch(Exception $e)
        {
            echo $e->getMessage() . "\n";
            return NULL;
        }
    }

    public function registrarMejorPrecioVueloAction($fecha, $origen, $destino)
    {
        $em = $this->getDoctrine()->getManager();

        $registro = new Registropreciovuelo();

        $datetimeVuelo = new DateTime($fecha);

        $registro->setRpvFecha(new DateTime('now'));
        $registro->setRpvHora(new DateTime('now'));
        $registro->setRpvFechaVuelo($datetimeVuelo);

        $arreglo = PrecioVueloController::obtenerMejorPrecioVuelo($datetimeVuelo, $origen, $destino);

        $registro->setRpvPrecio(floatval($arreglo['bestPrice']));
        $registro->setRpvAerolinea($arreglo['airline']);
        $registro->setRpvNumeroEscalas($arreglo['scales']);

        $em->persist($registro);
        $em->flush();

        return new Response(json_encode($arreglo));
    }

}
