<?php

namespace Bonilla\AnalystBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReporteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('repAnio')
            ->add('repTrimestre')
            ->add('repPatrimonio')
            ->add('repUtilidadNeta')
            ->add('repEsp')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bonilla\AnalystBundle\Entity\Reporte'
        ));
    }

    public function getName()
    {
        return 'bonilla_analystbundle_reportetype';
    }
}
