<?php

namespace Bonilla\AnalystBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bonilla\AnalystBundle\Entity\Registropreciovuelo
 */
class Registropreciovuelo
{
    /**
     * @var \DateTime $rpvFecha
     */
    private $rpvFecha;

    /**
     * @var \DateTime $rpvHora
     */
    private $rpvHora;

    /**
     * @var \DateTime $rpvFechaVuelo
     */
    private $rpvFechaVuelo;

    /**
     * @var string $rpvAerolinea
     */
    private $rpvAerolinea;

    /**
     * @var integer $rpvNumeroEscalas
     */
    private $rpvNumeroEscalas;

    /**
     * @var float $rpvPrecio
     */
    private $rpvPrecio;

    /**
     * @var integer $id
     */
    private $id;


    /**
     * Set rpvFecha
     *
     * @param \DateTime $rpvFecha
     * @return Registropreciovuelo
     */
    public function setRpvFecha($rpvFecha)
    {
        $this->rpvFecha = $rpvFecha;
    
        return $this;
    }

    /**
     * Get rpvFecha
     *
     * @return \DateTime 
     */
    public function getRpvFecha()
    {
        return $this->rpvFecha;
    }

    /**
     * Set rpvHora
     *
     * @param \DateTime $rpvHora
     * @return Registropreciovuelo
     */
    public function setRpvHora($rpvHora)
    {
        $this->rpvHora = $rpvHora;
    
        return $this;
    }

    /**
     * Get rpvHora
     *
     * @return \DateTime 
     */
    public function getRpvHora()
    {
        return $this->rpvHora;
    }

    /**
     * Set rpvFechaVuelo
     *
     * @param \DateTime $rpvFechaVuelo
     * @return Registropreciovuelo
     */
    public function setRpvFechaVuelo($rpvFechaVuelo)
    {
        $this->rpvFechaVuelo = $rpvFechaVuelo;
    
        return $this;
    }

    /**
     * Get rpvFechaVuelo
     *
     * @return \DateTime 
     */
    public function getRpvFechaVuelo()
    {
        return $this->rpvFechaVuelo;
    }

    /**
     * Set rpvAerolinea
     *
     * @param string $rpvAerolinea
     * @return Registropreciovuelo
     */
    public function setRpvAerolinea($rpvAerolinea)
    {
        $this->rpvAerolinea = $rpvAerolinea;
    
        return $this;
    }

    /**
     * Get rpvAerolinea
     *
     * @return string 
     */
    public function getRpvAerolinea()
    {
        return $this->rpvAerolinea;
    }

    /**
     * Set rpvNumeroEscalas
     *
     * @param integer $rpvNumeroEscalas
     * @return Registropreciovuelo
     */
    public function setRpvNumeroEscalas($rpvNumeroEscalas)
    {
        $this->rpvNumeroEscalas = $rpvNumeroEscalas;
    
        return $this;
    }

    /**
     * Get rpvNumeroEscalas
     *
     * @return integer 
     */
    public function getRpvNumeroEscalas()
    {
        return $this->rpvNumeroEscalas;
    }

    /**
     * Set rpvPrecio
     *
     * @param float $rpvPrecio
     * @return Registropreciovuelo
     */
    public function setRpvPrecio($rpvPrecio)
    {
        $this->rpvPrecio = $rpvPrecio;
    
        return $this;
    }

    /**
     * Get rpvPrecio
     *
     * @return float 
     */
    public function getRpvPrecio()
    {
        return $this->rpvPrecio;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
