<?php

namespace Bonilla\AnalystBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bonilla\AnalystBundle\Entity\Especie
 */
class Especie
{
    /**
     * @var string $espNombre
     */
    private $espNombre;

    /**
     * @var integer $espId
     */
    private $espId;

public function __toString()
    {
   return $this->espNombre;
}

    /**
     * Set espNombre
     *
     * @param string $espNombre
     * @return Especie
     */
    public function setEspNombre($espNombre)
    {
        $this->espNombre = $espNombre;
    
        return $this;
    }

    /**
     * Get espNombre
     *
     * @return string 
     */
    public function getEspNombre()
    {
        return $this->espNombre;
    }

    /**
     * Get espId
     *
     * @return integer 
     */
    public function getEspId()
    {
        return $this->espId;
    }
    /**
     * @var integer $id
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}