<?php

namespace Bonilla\AnalystBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bonilla\AnalystBundle\Entity\Registrotemperatura
 */
class Registrotemperatura
{
    /**
     * @var \DateTime $temFecha
     */
    private $temFecha;

    /**
     * @var integer $temHora
     */
    private $temHora;

    /**
     * @var float $temTemperatura
     */
    private $temTemperatura;

    /**
     * @var integer $id
     */
    private $id;


    /**
     * Set temFecha
     *
     * @param \DateTime $temFecha
     * @return Registrotemperatura
     */
    public function setTemFecha($temFecha)
    {
        $this->temFecha = $temFecha;
    
        return $this;
    }

    /**
     * Get temFecha
     *
     * @return \DateTime 
     */
    public function getTemFecha()
    {
        return $this->temFecha;
    }

    /**
     * Set temHora
     *
     * @param integer $temHora
     * @return Registrotemperatura
     */
    public function setTemHora($temHora)
    {
        $this->temHora = $temHora;
    
        return $this;
    }

    /**
     * Get temHora
     *
     * @return integer 
     */
    public function getTemHora()
    {
        return $this->temHora;
    }

    /**
     * Set temTemperatura
     *
     * @param float $temTemperatura
     * @return Registrotemperatura
     */
    public function setTemTemperatura($temTemperatura)
    {
        $this->temTemperatura = $temTemperatura;
    
        return $this;
    }

    /**
     * Get temTemperatura
     *
     * @return float 
     */
    public function getTemTemperatura()
    {
        return $this->temTemperatura;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}