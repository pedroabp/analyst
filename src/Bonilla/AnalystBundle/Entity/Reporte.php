<?php

namespace Bonilla\AnalystBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bonilla\AnalystBundle\Entity\Reporte
 */
class Reporte
{
    /**
     * @var integer $repAnio
     */
    private $repAnio;

    /**
     * @var integer $repTrimestre
     */
    private $repTrimestre;

    /**
     * @var float $repPatrimonio
     */
    private $repPatrimonio;

    /**
     * @var float $repUtilidadNeta
     */
    private $repUtilidadNeta;

    /**
     * @var integer $repId
     */
    private $repId;

    /**
     * @var Bonilla\AnalystBundle\Entity\Especie
     */
    private $repEsp;


    /**
     * Set repAnio
     *
     * @param integer $repAnio
     * @return Reporte
     */
    public function setRepAnio($repAnio)
    {
        $this->repAnio = $repAnio;
    
        return $this;
    }

    /**
     * Get repAnio
     *
     * @return integer 
     */
    public function getRepAnio()
    {
        return $this->repAnio;
    }

    /**
     * Set repTrimestre
     *
     * @param integer $repTrimestre
     * @return Reporte
     */
    public function setRepTrimestre($repTrimestre)
    {
        $this->repTrimestre = $repTrimestre;
    
        return $this;
    }

    /**
     * Get repTrimestre
     *
     * @return integer 
     */
    public function getRepTrimestre()
    {
        return $this->repTrimestre;
    }

    /**
     * Set repPatrimonio
     *
     * @param float $repPatrimonio
     * @return Reporte
     */
    public function setRepPatrimonio($repPatrimonio)
    {
        $this->repPatrimonio = $repPatrimonio;
    
        return $this;
    }

    /**
     * Get repPatrimonio
     *
     * @return float 
     */
    public function getRepPatrimonio()
    {
        return $this->repPatrimonio;
    }

    /**
     * Set repUtilidadNeta
     *
     * @param float $repUtilidadNeta
     * @return Reporte
     */
    public function setRepUtilidadNeta($repUtilidadNeta)
    {
        $this->repUtilidadNeta = $repUtilidadNeta;
    
        return $this;
    }

    /**
     * Get repUtilidadNeta
     *
     * @return float 
     */
    public function getRepUtilidadNeta()
    {
        return $this->repUtilidadNeta;
    }

    /**
     * Get repId
     *
     * @return integer 
     */
    public function getRepId()
    {
        return $this->repId;
    }

    /**
     * Set repEsp
     *
     * @param Bonilla\AnalystBundle\Entity\Especie $repEsp
     * @return Reporte
     */
    public function setRepEsp(\Bonilla\AnalystBundle\Entity\Especie $repEsp = null)
    {
        $this->repEsp = $repEsp;
    
        return $this;
    }

    /**
     * Get repEsp
     *
     * @return Bonilla\AnalystBundle\Entity\Especie 
     */
    public function getRepEsp()
    {
        return $this->repEsp;
    }
    /**
     * @var integer $id
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}